## Continuous Integration Demo Sender Application

Demo "sender application" used for the talk "Continuous integration and delivery: from zero to hero with TeamCity, Docker and Kubernetes". The application is meant to be used in sinergy with the "receiver application".