# See: https://spring.io/guides/gs/spring-boot-docker/
FROM openjdk:8-jdk-alpine

COPY ./target/ci-demo-sender.jar /usr/src/ci-demo-sender.jar

WORKDIR /usr/src/

EXPOSE 8090

# See: https://wiki.apache.org/tomcat/HowTo/FasterStartUp#Entropy_Source
CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "ci-demo-sender.jar"]
