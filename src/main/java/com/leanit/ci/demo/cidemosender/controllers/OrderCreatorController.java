package com.leanit.ci.demo.cidemosender.controllers;

import java.time.ZonedDateTime;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.leanit.ci.demo.utilitylib.domaindto.OrderDto;
import com.leanit.ci.demo.utilitylib.rabbit.IBus;

@RestController
public class OrderCreatorController {

	@Autowired private IBus bus;
	@Autowired private Gson gson;
	
	private static final Logger log = LoggerFactory.getLogger(OrderCreatorController.class);

	@RequestMapping(value="/orders", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> createNewOrder() {
		String orderId = UUID.randomUUID().toString();
		
		bus.publish(new OrderDto(
				orderId, 
				String.format("Order %s created at %s", orderId, ZonedDateTime.now().toString()))
		);
		
		String statusMsg = String.format("Created order %s and sucessfully dispatched for processing", orderId);
		log.info(statusMsg);
		return ResponseEntity.ok(gson.toJson(statusMsg));
	}
}