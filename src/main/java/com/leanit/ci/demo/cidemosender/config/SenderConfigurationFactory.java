package com.leanit.ci.demo.cidemosender.config;

import com.leanit.ci.demo.utilitylib.environment.DemoEnvironment;
import com.leanit.common.utilitylib.rabbit.utils.RabbitConfiguration;
import com.leanit.common.utilitylib.rabbit.utils.RabbitConstants;

public class SenderConfigurationFactory {

	SenderConfigurationDto buildConfiguration(DemoEnvironment demoEnv) {
		SenderConfigurationDto returnValue = null;

		switch(demoEnv) {
			case LOCAL:
				returnValue = this.doForLocal();
				break;
			case TOMCAT:
				returnValue = this.doForTomcat();
				break;
			case KUBERNETES:
				returnValue = this.doForKubernetes();
				break;
			default:
				throw new RuntimeException("Unknown environment");
		}
		
		return returnValue;
	}
	
	private SenderConfigurationDto doForLocal() {
		return new SenderConfigurationDto(new RabbitConfiguration("localhost", 5672, "cidemo", "cidemo123", RabbitConstants.QUEUE_NAME));
	}

	private SenderConfigurationDto doForTomcat() {
		return new SenderConfigurationDto(new RabbitConfiguration("localhost", 5672, "cidemo", "cidemo123", RabbitConstants.QUEUE_NAME));
	}
	
	private SenderConfigurationDto doForKubernetes() {
		return new SenderConfigurationDto(new RabbitConfiguration("rabbitqueue-svc", 5672, "guest", "guest", RabbitConstants.QUEUE_NAME));
	}
}