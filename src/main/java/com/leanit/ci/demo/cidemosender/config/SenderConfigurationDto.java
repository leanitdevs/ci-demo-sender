package com.leanit.ci.demo.cidemosender.config;

import com.leanit.common.utilitylib.rabbit.utils.RabbitConfiguration;

public class SenderConfigurationDto {
	public RabbitConfiguration rabbitConfiguration;

	public SenderConfigurationDto(RabbitConfiguration rabbitConfiguration) {
		this.rabbitConfiguration = rabbitConfiguration;
	}
}
