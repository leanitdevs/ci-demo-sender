package com.leanit.ci.demo.cidemosender.config;

import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.gson.Gson;
import com.leanit.ci.demo.utilitylib.environment.DemoEnvironment;
import com.leanit.ci.demo.utilitylib.environment.EnvironmentDetector;
import com.leanit.ci.demo.utilitylib.rabbit.IBus;
import com.leanit.ci.demo.utilitylib.rabbit.IRabbitBusManager;
import com.leanit.ci.demo.utilitylib.rabbit.RabbitBusManager;

@Configuration
public class AppConfig {

	private IRabbitBusManager busManager;
	private SenderConfigurationFactory configurationFactory;
	private EnvironmentDetector environmentDetector;

	public AppConfig () {
		this(new SenderConfigurationFactory(), new EnvironmentDetector());
	}
	
	public AppConfig(SenderConfigurationFactory configurationFactory, EnvironmentDetector environmentDetector) {
		this.configurationFactory = configurationFactory;
		this.environmentDetector = environmentDetector;
	}
	
	@Bean 
	public IBus getBus() {
		busManager = new RabbitBusManager(this.getConfiguration().rabbitConfiguration);
		return busManager.start();
	}
	
	@Bean
	public DemoEnvironment getCurrentEnvironment() {
		return environmentDetector.detect();
	}
	
	@Bean
	public SenderConfigurationDto getConfiguration() {
		return configurationFactory.buildConfiguration(getCurrentEnvironment());
	}
	
	@Bean
	public Gson getGson() {
		return new Gson(); // Customization can be added here
	}
	
	@PreDestroy
	public void destroy() {
		busManager.stop();
	}
}