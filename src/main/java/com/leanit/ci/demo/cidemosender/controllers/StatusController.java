package com.leanit.ci.demo.cidemosender.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

@RestController
@RequestMapping("/status")
public class StatusController {

	private static final Logger log = LoggerFactory.getLogger(StatusController.class);

	@Autowired private Gson gson;
	
	@RequestMapping(value="/check", method = RequestMethod.GET)
	public ResponseEntity<String> getStatus() {
		log.info("/status/check endpoint called");

		return ResponseEntity.ok(gson.toJson("OK"));
	}
}