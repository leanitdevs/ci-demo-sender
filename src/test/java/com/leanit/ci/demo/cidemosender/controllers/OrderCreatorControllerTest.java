package com.leanit.ci.demo.cidemosender.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.leanit.ci.demo.utilitylib.rabbit.IBus;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderCreatorController.class)
public class OrderCreatorControllerTest {

	private static final String ORDER_CREATION_URL = "/orders";
	@MockBean private IBus bus;
	@Autowired private MockMvc mockMvc;
	
	@Test
	public void testOderCreation() throws Exception {
		mockMvc.perform(
				post(ORDER_CREATION_URL)
			.accept(MediaType.APPLICATION_JSON)
			.contentType(MediaType.APPLICATION_JSON))
			.andDo(MockMvcResultHandlers.print())
			.andExpect(status().isOk());
		
		verify(bus, times(1)).publish(any());
	}
}