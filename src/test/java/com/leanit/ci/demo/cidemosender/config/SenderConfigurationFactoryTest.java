package com.leanit.ci.demo.cidemosender.config;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import com.leanit.ci.demo.utilitylib.environment.DemoEnvironment;

public class SenderConfigurationFactoryTest {

	@Test
	public void testConfigForLocalshot() {
		SenderConfigurationDto configDto = new SenderConfigurationFactory().buildConfiguration(DemoEnvironment.LOCAL);
		
		assertThat(configDto.rabbitConfiguration.host, is("localhost"));
	}

	@Test
	public void testConfigForTomcat() {
		SenderConfigurationDto configDto = new SenderConfigurationFactory().buildConfiguration(DemoEnvironment.TOMCAT);
		
		assertThat(configDto.rabbitConfiguration.host, is("localhost"));
	}

	@Test
	public void testConfigForK8s() {
		SenderConfigurationDto configDto = new SenderConfigurationFactory().buildConfiguration(DemoEnvironment.KUBERNETES);
		
		assertThat(configDto.rabbitConfiguration.host, is("rabbitqueue-svc"));
	}
}